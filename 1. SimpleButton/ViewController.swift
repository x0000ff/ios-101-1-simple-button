//
//  ViewController.swift
//  1. SimpleButton
//
//  Created by x0000ff on 11/07/15.
//
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myButton: UIButton!
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBAction func buttonTapped(sender: AnyObject) {

        // Объявляем вспомогательную переменную
        let message = "Ой, меня нажали!"
        
        // Выводим на консоль
        println(message)
        
        // А теперь поменяем заголовок / текст кнопки
        // А как???
        // Для этого нам нужно что-то, что бы указывала / связывало
        // нашу кнопку с нашим же кодом, верно?
        
        // Для этого существуют Outlet'ы
        myButton.setTitle(message, forState: UIControlState.Normal)
    
        // А еще можно такое:
        // Здесь мы устанавливаем кнопке красный цвет для НОРМАЛЬНОГО состояния
        myButton.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        
        // И коричневый) цвет текста для НАЖАТОГО состояния
        myButton.setTitleColor(UIColor.brownColor(), forState: UIControlState.Highlighted)
        
        
        // А как нам сделать тоже самое с текстом Label?
        // Добавим Outlet
        
        // Изменим цвет текста
        myLabel.textColor = UIColor.blueColor()
        
        // Добавим окантовку цветом "grayColor" и толщиной 2 пикселя
        myLabel.layer.borderColor = UIColor.grayColor().CGColor
        myLabel.layer.borderWidth = 2
        
        // А еще закруглим углы на 4 пикселей
        myLabel.layer.cornerRadius = 4
        
        // Всё, что можно изменить в Interface Builder
        // ( Это то окошко, в котором мы располагаем визуальные элементы)
        // можно изменить и в коде!
        
        // текст
        myLabel.text = "Йиииихааааа!!!"

        // Выравнивание
        myLabel.textAlignment = NSTextAlignment.Left
        
        // Поиграйтесь в другими параметрами кнопки и метки (UILabel)
        // Удачи ;)
    
    }
    
}

